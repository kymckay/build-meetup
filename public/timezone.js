// ES6 feature, supported by 95.51% of browsers used (2021-06-22)
const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;

// Need to embed the full iframe element so that the URI timezone component is picked up on load
const embed = `<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=${encodeURIComponent(tz)}&amp;src=MDc2Y3JkcHBtNzdxcDM0c2dodWRtNG8xNTRAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;mode=AGENDA&amp;showTz=1&amp;showCalendars=1&amp;showTabs=0&amp;showDate=1&amp;showNav=1&amp;showTitle=1&amp;showPrint=1" frameborder="0" scrolling="no"></iframe>`

document.addEventListener("DOMContentLoaded", () => {
    const container = document.getElementById("gcal-embed");
    container.innerHTML = embed;
});