#!/usr/bin/python3

import pandas as pd
import matplotlib.pyplot as plt

print("Plotting stats on build meetup attendance")

# Load data
df = pd.read_csv("Build_Order_data-07.06.21_edited.csv")
n_participants = len(df.index)
print(n_participants)

# print(list(df.columns))

# Set up plot properties

plt.rcParams.update({
    "figure.facecolor":  (1.0, 0.0, 0.0, 0),  # red   with alpha = 30%
    "axes.facecolor":    (1.0, 1.0, 1.0, 1),  # green with alpha = 50%
    "savefig.facecolor": (0.0, 0.0, 1.0, 0),  # blue  with alpha = 20%
})

text_colour = 'white'
plt.rcParams['text.color'] = text_colour
plt.rcParams['axes.labelcolor'] = text_colour
plt.rcParams['xtick.color'] = text_colour
plt.rcParams['ytick.color'] = text_colour

# Count languages
Lang_ColumnNames = []
Lang_n_yes = []

ColumnName = "Lang_C"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_C++"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_C#"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Go"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Java"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Javascript"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Python"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Ruby"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Rust"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Lang_Scala"
Lang_ColumnNames.append(ColumnName)
Lang_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

Lang_n_no = [None] * len(Lang_n_yes)
Lang_percent_useage = [None] * len(Lang_n_yes)
for i in range(len(Lang_n_yes)):
    Lang_n_no[i] = n_participants - Lang_n_yes[i]
    Lang_percent_useage[i] = 100 * Lang_n_yes[i]/n_participants

Lang_pretty_names = ["C", "C++", "C#", "Go", "Java", "Javascript", "Python", "Ruby", "Rust", "Scala"]
print(Lang_pretty_names)
print(Lang_n_yes)
print(Lang_n_no)
print(Lang_percent_useage)

lang_stats = pd.DataFrame(list(zip(Lang_pretty_names, Lang_n_yes, Lang_n_no, Lang_percent_useage)), columns = ["Name", "N using", "N not using", "Percent useage"])
lang_stats.to_csv("Lang_stats.csv", index=False)

plt.pie(Lang_n_yes, wedgeprops=dict(width=0.5), labels=Lang_pretty_names)
plt.savefig("Language_pie_chart.png")
#plt.show()
plt.clf()

x_pos = [i for i, _ in enumerate(Lang_pretty_names)]
plt.barh(x_pos, Lang_percent_useage)
plt.xlabel("% useage")
plt.yticks(x_pos, Lang_pretty_names)
plt.tight_layout()
plt.savefig("Language_useages.png")
#plt.show()
plt.clf()

# Count build systems
Build_ColumnNames = []
Build_n_yes = []


ColumnName = "Build_Bazel"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Buck"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Cmake"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Gradle"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Goma"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Maven"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_MSBuild"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Pants"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Recc"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build_Soong"
Build_ColumnNames.append(ColumnName)
Build_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

Build_n_no = [None] * len(Build_n_yes)
Build_percent_useage = [None] * len(Build_n_yes)
for i in range(len(Build_n_yes)):
    Build_n_no[i] = n_participants - Build_n_yes[i]
    Build_percent_useage[i] = 100 * Build_n_yes[i] / n_participants

Build_pretty_names = ["Bazel", "Buck", "CMake", "Gradle", "Goma", "Maven", "MSBuild", "Pants", "Recc", "Soong"]
print(Build_pretty_names)
print(Build_n_yes)
print(Build_n_no)
print(Build_percent_useage)

build_stats = pd.DataFrame(list(zip(Build_pretty_names, Build_n_yes, Build_n_no, Build_percent_useage)), columns = ["Name", "N using", "N not using", "Percent useage"])
build_stats.to_csv("Build_stats.csv", index=False)

plt.pie(Build_n_yes, wedgeprops=dict(width=0.5), labels=Build_pretty_names)
plt.savefig("Build_system_pie_chart.png")
plt.clf()
#plt.show()

x_pos = [i for i, _ in enumerate(Build_pretty_names)]
plt.barh(x_pos, Build_percent_useage)
plt.xlabel("% useage")
plt.yticks(x_pos, Build_pretty_names)
plt.tight_layout()
plt.savefig("Build_useages.png")
#plt.show()
plt.clf()


# Plot remote build useage
Remote_ColumnNames = []
Remote_n_yes = []

ColumnName = "Using Remote Caching"
Remote_ColumnNames.append(ColumnName)
Remote_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Using Remote Execution"
Remote_ColumnNames.append(ColumnName)
Remote_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Evaluating Remote Caching"
Remote_ColumnNames.append(ColumnName)
Remote_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Evaluating Remote Execution"
Remote_ColumnNames.append(ColumnName)
Remote_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

Remote_percent_useage = [None]*len(Remote_n_yes)
for i in range(len(Remote_n_yes)):
	Remote_percent_useage[i] = 100*Remote_n_yes[i] / n_participants

x_pos = [i for i, _ in enumerate(Remote_ColumnNames)]
plt.figure(figsize=(8, 4.8))
plt.barh(x_pos, Remote_percent_useage)
plt.xlabel("% useage")
plt.yticks(x_pos, Remote_ColumnNames)
plt.tight_layout()
plt.savefig("Remote_cache_exec_useages.png")
#plt.show()
plt.clf()


# Plot topic interests
Topics_ColumnNames = []
Topics_n_yes = []

ColumnName = "Build performance optimization"
Topics_ColumnNames.append(ColumnName)
Topics_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Build system migration"
Topics_ColumnNames.append(ColumnName)
Topics_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Remote execution"
Topics_ColumnNames.append(ColumnName)
Topics_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Bazel rules"
Topics_ColumnNames.append(ColumnName)
Topics_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

ColumnName = "Remote Development"
Topics_ColumnNames.append(ColumnName)
Topics_n_yes.append(df.loc[df[ColumnName] == "Yes", ColumnName].size)

Topics_percent_useage = [None]*len(Topics_n_yes)
for i in range(len(Topics_n_yes)):
	Topics_percent_useage[i] = 100*Topics_n_yes[i] / n_participants

x_pos = [i for i, _ in enumerate(Topics_ColumnNames)]
plt.figure(figsize=(8, 4.8))
plt.barh(x_pos, Topics_percent_useage)
plt.xlabel("% interest")
plt.yticks(x_pos, Topics_ColumnNames)
plt.tight_layout()
plt.savefig("Topics_interest.png")
#plt.show()
plt.clf()

